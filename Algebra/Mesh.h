#pragma once
#include <vector>
#include "Algebra.h"


class Mesh {
// variables
public:	
	std::vector<algebra::Vec3<float>> points;
	std::vector<algebra::Vec3<float>> normals;
	std::vector<algebra::Vec2<float>> UVs;
	std::vector<int> normalsIndex;
	std::vector<int> uvIndex;
	std::vector<int> pointsIndex;
// methods
public:
	std::vector<algebra::Vec4<float>> project(const std::vector<algebra::Vec3<float>>& points, int w, int h)const;
	std::vector<algebra::Vec3<float>> view(const std::vector<algebra::Vec3<float>>& points,const algebra::Matrix4<float>& view)const;
	bool ExtractObj(const std::string& path);
	
	void ComputeFlat(); // convert all 3 vectors (points,normals,UVs) to Vertex vector
	const std::vector<float>& GetFlatPositions() const;
	const std::vector<float>& GetFlatNormals() const;
	const std::vector<float>& GetFlatTextures() const;
	const std::vector<unsigned int>& GetFlatIndices() const;
// private variables
private:
	std::vector<float> flat_positions_ = {};
	std::vector<float> flat_normals_ = {};
	std::vector<float> flat_textures_ = {};
	std::vector<unsigned int> flat_indices_ = {};

};
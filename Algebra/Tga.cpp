#include "Tga.h"
#include <fstream>
#include <iostream>


struct tgaHeader {
	unsigned char iDLength;
	unsigned char colormapType;
	unsigned char imageType;
	unsigned char colorMapSpecification[5];

	// on a s�par� l'Image specification pour avoir plus facilement la length et la width
	unsigned short xOrigin;
	unsigned short yOrigin;
	unsigned short width;
	unsigned short height;
	unsigned char depth;
	unsigned char imageDescriptor;
};

// pour lire le haeder dans un cout
std::ostream& operator<<(std::ostream& os, const tgaHeader& header)
{
	os << (int)header.iDLength << '/' << (int)header.colormapType << '/' << (int)header.imageType << '/'
		<< (int)header.xOrigin << '/' << (int)header.yOrigin << '/' << (int)header.width << '/'
		<< (int)header.height << '/' << (int)header.depth << '/' << (int)header.imageDescriptor << '/';
	return os;
}


bool Tga::load(ImageData& data, const std::string& path) {
	std::ifstream ifs;
	tgaHeader header;

	// open file
	ifs.open(path, std::ifstream::in | std::ifstream::binary);

	if (!ifs.is_open()) {
		return false;
	}

	ifs.seekg(0, ifs.end);
	size = ifs.tellg();
	ifs.seekg(0, ifs.beg);

	// read file
	ifs.read((char*)&header, sizeof(header));

	data.x = header.width;

	data.y = header.height;

	data.img.resize((size - sizeof(header))/3);

	struct Pixel {
		unsigned char r, g, b;
	};

	std::vector<Pixel> vecTemp;
	vecTemp.resize((size - sizeof(header))/3);

	ifs.read((char*)&vecTemp[0], size - sizeof(header));
	int i = 0;
	for (; i < vecTemp.size(); ++i) {
		data.img[i].x = vecTemp[i].b / 255.0f;
		data.img[i].y = vecTemp[i].g / 255.0f;
		data.img[i].z = vecTemp[i].r / 255.0f;
		data.img[i].w = 1;
	}
	std::cout << i <<std::endl;
	return true;
}

bool Tga::save(const ImageData& data, const std::string& path) {
	std::ofstream ofs;
	tgaHeader header;


	// open file
	ofs.open(path, std::ofstream::out | std::ofstream::trunc | std::ofstream::binary);

	if (!ofs.is_open()) {
		return false;
	}

	// remplir les variables du header avec 0
	memset(&header, 0, sizeof(header));
	header.width = data.x;
	header.height = data.y;
	header.depth = 24;
	header.imageType = 2;


	// write header
	ofs.write((char*)&header, sizeof(header));
	// write pixel vector
	ofs.write((char*)&data.img[0], size - sizeof(header));

	ofs.close();



	return true;
}
#include "Camera.h"
#include "Algebra.h"


algebra::Matrix4<float> Camera::View() const {
	algebra::Vec3<float> forwardNormalize = (target-position).normalize();
	algebra::Vec3<float> upNormalize = (up-(forwardNormalize*up)).normalize();
	algebra::Vec3<float> rightNormalize = (forwardNormalize ^ upNormalize).normalize();

	return{
		rightNormalize.x,	rightNormalize.y,	rightNormalize.z,	0,
		upNormalize.x,		upNormalize.y,		upNormalize.z,		0,
		forwardNormalize.x,	forwardNormalize.y,	forwardNormalize.z,	0,
		position.x,			position.y,			position.z,			1
	};
}

algebra::Matrix4<float> Camera::ViewInv() const {
	algebra::Vec3<float> forwardNormalize = (target - position).normalize();
	algebra::Vec3<float> upNormalize = (up - (forwardNormalize*up)).normalize();
	algebra::Vec3<float> rightNormalize = (forwardNormalize ^ upNormalize).normalize();

	std::cout << "forwardNormalize: " << forwardNormalize << std::endl;
	std::cout << "upNormalize: " << upNormalize << std::endl;
	std::cout << "rightNormalize: " << rightNormalize << std::endl;

	return{
		forwardNormalize.x,			  rightNormalize.x,			  upNormalize.x,				0,
		forwardNormalize.y,			  rightNormalize.y,			  upNormalize.y,				0,
		forwardNormalize.z,			  rightNormalize.z,			  upNormalize.z,				0,
		-(position*forwardNormalize), -(position*rightNormalize), -(position * upNormalize),	1
	};
}
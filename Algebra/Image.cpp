#include "Image.h"
#include "Algebra.h"
#include "Mesh.h"
#include "Tga.h"
#include <vector>
#include <assert.h>


void ImageData::FindTopVertex(Vertex v1, Vertex v2, Vertex v3)
{
	// FIND SQUARE THAT CONTAINS OUR TRIANGLE

	/* FIND TOP MIDDLE AND LOW VERTEX */
	Vertex vTop;
	Vertex vMiddle;
	Vertex vLow;

	vTop = v1;
	// if the second vertex is higher than the first, v1 go in the middle and v2 in the top
	if (v2.screenPosition.y > vTop.screenPosition.y) {
		vMiddle = vTop;
		vTop = v2;
	}
	// else v2 goes directly in the middle
	else {
		vMiddle = v2;
	}
}

void ImageData::FillTriangle(const ImageData& imgTexture, const Mesh& mesh, const float& windowWidth, std::vector<float>& zBuffer,
							 const Vertex& a, const Vertex& b, const Vertex& c)
{



	algebra::Vec4<float> square;
	square = FindSquare(a, b, c);
	
	algebra::Vec3<float> light_pos = { 0.0f,0.0f,-1.0f };
	light_pos = light_pos.normalize();
	
	// check if the normals are the same between all vertex
	bool hasIdenticalNormals;

	if (a.normal == b.normal &&
		b.normal == c.normal &&
		c.normal == a.normal) {

		hasIdenticalNormals = true;
	}
	else {
		hasIdenticalNormals = false;
	}

	
	algebra::Vec3<float> normal;

	for (int i = square.y; i < square.w; i++) {
		for (int j = square.x; j < square.z; j++) {

			float s = BarycentricS(a, b, c, algebra::Vec3<float>((float)j, (float)i, 0));
			float t = BarycentricT(a, b, c, algebra::Vec3<float>((float)j, (float)i, 0));
			float u = 1-(s + t);
			
			if (hasIdenticalNormals) {
				normal = a.normal;
			}
			else {
				normal = a.normal*s + b.normal*t + c.normal*u;
			}
			
			float shade = light_pos * normal;

			if (isInside(s,t)) {

				float z = a.screenPosition.z*s + b.screenPosition.z*t + c.screenPosition.z*u;

				// prendre la largeur et hauteur de l'image texture et pas de l'�cran.
				if (z < zBuffer[i*windowWidth+j]) {
					zBuffer[i*windowWidth + j] = z;

					float uT = a.uv.x*s + b.uv.x*t + c.uv.x*u;
					float vT = a.uv.y*s + b.uv.y*t + c.uv.y*u;

					assert(uT >= 0 && uT <= 1);
					assert(vT >= 0 && vT <= 1);

					//printf("%f, %f\n", uT, vT);

					uT *= imgTexture.x;
					vT *= imgTexture.y;


					

					// PROBLEME D'INDEX ! CA NE PREND PAS LA BONNE COULEUR
					int index = static_cast<int>(vT) * imgTexture.x + static_cast<int>(uT);

					if (index >= imgTexture.img.size()) {
						index = imgTexture.img.size()-1;
					}

					algebra::Vec4<float> color = imgTexture.img[index]*shade;
					
					drawPixel(j, i, color);
				}
			}
		}
	}
}

float ImageData::BarycentricS(const Vertex & a, const Vertex & b, const Vertex & c, const algebra::Vec3<float>& p) const
{
	// Get Barycentric S

	float denum = 1.f / ((b.screenPosition.y - c.screenPosition.y) *
		(a.screenPosition.x - c.screenPosition.x) + (c.screenPosition.x - b.screenPosition.x) *
		(a.screenPosition.y - c.screenPosition.y));

	return ((b.screenPosition.y - c.screenPosition.y) * (p.x - c.screenPosition.x) +
		(c.screenPosition.x - b.screenPosition.x) * (p.y - c.screenPosition.y)) * denum;


}

float ImageData::BarycentricT(const Vertex & a, const Vertex & b, const Vertex & c, const algebra::Vec3<float>& p) const
{
	// Get Barycentric T
	float denum = 1.f / ((b.screenPosition.y - c.screenPosition.y) *
		(a.screenPosition.x - c.screenPosition.x) + (c.screenPosition.x - b.screenPosition.x) *
		(a.screenPosition.y - c.screenPosition.y));

	return ((c.screenPosition.y - a.screenPosition.y) * (p.x - c.screenPosition.x) +
		(a.screenPosition.x - c.screenPosition.x) * (p.y - c.screenPosition.y)) * denum;

}

bool ImageData::isInside(const float& s, const float& t) const
{
	
	if (0 > s || s > 1) {
		return false;
	}
	
	if (0 > t || t > 1) {
		return false;
	}
	
	if (0 > t+s || t+s > 1) {
		return false;
	}

	return true;
	
	
}

algebra::Vec4<float> ImageData::FindSquare(Vertex v1, Vertex v2, Vertex v3)
{
	// FIND SQUARE THAT CONTAINS OUR TRIANGLE

	/* MAX Y & MIN Y*/
	
	float maxY = v1.screenPosition.y;
	float minY;
	float middleTempY;

	// if the second vertex is higher than the first, v1 go in the middle and v2 in the top
	if (v2.screenPosition.y > maxY) {
		middleTempY = maxY;
		maxY = v2.screenPosition.y;
	}
	// else v2 goes directly in the middle
	else {
		middleTempY = v2.screenPosition.y;
	}
	// if the third vertex is higher it push down the 2 other vertex on the bottom and take the higher place
	if (v3.screenPosition.y > maxY) {
		minY = middleTempY;
		middleTempY = maxY;
		maxY = v3.screenPosition.y;
	}
	// else if the third is smaller than the top one but higher than the middle one, it take the middle place and push down the last one
	else if (v3.screenPosition.y > middleTempY) {
		minY = middleTempY;
		middleTempY = v3.screenPosition.y;
	}
	// otherwise it go the the lowest place
	else {
		minY = v3.screenPosition.y;
	}

	/* MAX X & MIN X*/

	float maxX = v1.screenPosition.x;
	float minX;
	float middleTempX;

	// if the second vertex is higher than the first, v1 go in the middle and v2 in the top
	if (v2.screenPosition.x > maxX) {
		middleTempX = maxX;
		maxX = v2.screenPosition.x;
	}
	// else v2 goes directly in the middle
	else {
		middleTempX = v2.screenPosition.x;
	}
	// if the third vertex is higher it push down the 2 other vertex on the bottom and take the higher place
	if (v3.screenPosition.x > maxX) {
		minX = middleTempX;
		middleTempX = maxX;
		maxX = v3.screenPosition.x;
	}
	// else if the third is smaller than the top one but higher than the middle one, it take the middle place and push down the last one
	else if (v3.screenPosition.x > middleTempX) {
		minX = middleTempX;
		middleTempX = v3.screenPosition.x;
	}
	// otherwise it go the the lowest place
	else {
		minX = v3.screenPosition.x;
	}


	return algebra::Vec4<float>{minX, minY, maxX, maxY};
}

void ImageData::drawLine(int x1, int y1, int x2, int y2, algebra::Vec4<float> startColorPixel, algebra::Vec4<float> endColorPixel) {
	int dx, dy;
	float n;

	dx = abs(x2 - x1);
	dy = abs(y2 - y1);

	algebra::Vec4<float> pixelColor{};

	bool vertical = false;


	if (dx != 0) {
		double m = (float)dy / (float)dx;
		if (abs(m) > 1) {
			vertical = true;
		}
	}
	else {
		vertical = true;
	}

	if (!vertical) {
		if (y1 > y2) {
			std::swap(x1, x2);
			std::swap(y1, y2);
			std::swap(startColorPixel, endColorPixel);
		}

		if (x1 < x2) {
			for (int i = x1; i < x2; i++) {
				n = (float)i / (float)(x2 - 1);
				
				drawPixel(i, y1 + dy * (i - x1) / dx, pixelColor);
			}
		}
		else {
			std::swap(startColorPixel, endColorPixel);
			for (int i = x2; i < x1; i++) {
				n = (float)i / (float)(x1 - 1);
				drawPixel(i, y2 - dy * (i - x2) / dx, pixelColor);
			}
		}
	}
	else {
		if (x1 > x2) {
			std::swap(x1, x2);
			std::swap(y1, y2);
			std::swap(startColorPixel, endColorPixel);
		}
		if (y1 < y2) {
			for (int i = y1; i < y2; i++) {
				n = (float)i / (float)(y2 - 1);
				drawPixel(x1 + dx * (i - y1) / dy, i, pixelColor);
			}
		}
		else {
			std::swap(startColorPixel, endColorPixel);
			for (int i = y2; i < y1; i++) {
				n = (float)i / (float)(y1 - 1);
				drawPixel(x2 - dx * (i - y2) / dy, i, pixelColor);
			}
		}
	}
}

void ImageData::drawPixel(int x, int y, algebra::Vec4<float> color) {
	int windowWidth = 640;
	int windowHeight = 480;

	if (x < 0 || y<0) {
		return;
	}
	if (x > windowWidth-1 || y > windowHeight-1) {
		return;
	}
	
	img[x + (y*windowWidth)] = { color.x, color.y, color.z, 1 };
}
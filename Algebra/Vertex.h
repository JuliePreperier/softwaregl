#pragma once
#include "Algebra.h"

struct Vertex {
	algebra::Vec3<float> position_;
	algebra::Vec3<float> normal_;
	algebra::Vec2<float> texture_coord_;

};
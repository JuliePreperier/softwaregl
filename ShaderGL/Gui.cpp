#include "Gui.h"
#include <imgui.h>
#include "../ImGuiImpl/imgui_impl_sdl.h"
#include "../ImGuiImpl/imgui_impl_opengl3.h"


void Gui::GuiFrame(SDL_Window* sdlWin) {

	InitFrame(sdlWin);

	static float f = 0.0f;
	static int counter = 0;
	ImGuiIO& io = ImGui::GetIO();

	// Change font -> Work with freestyle : too long to install xD
	/*io.Fonts->AddFontDefault();
	io.Fonts->AddFontFromFileTTF("../../misc/fonts/Roboto-Medium.ttf", 16.0f);
	ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ITCKRIST.TTF", 18.0f,
		NULL, io.Fonts->GetGlyphRangesJapanese()
	);
	IM_ASSERT(font != NULL);*/

	const ImU32 flags =
		ImGuiWindowFlags_NoTitleBar |
		ImGuiWindowFlags_NoResize |
		ImGuiWindowFlags_NoMove;
	// the flags allow the window to have special behaviour

	// Define windows position
	float display_width = (float)io.DisplaySize.x;
	float display_height = (float)io.DisplaySize.y;
	ImGui::SetNextWindowPos(
		ImVec2(display_width, 0.f),
		ImGuiCond_Always,
		ImVec2(1.f, 0.f)
	);

	//Define windows size
	ImGui::SetNextWindowSize(
		ImVec2(340, 120),
		ImGuiCond_Always
	);



	ImGui::Begin("Frame rate", nullptr, flags);

	// PUT CONFIG HERE

	// Add color picker
	if (ImGui::CollapsingHeader("Colors")) {
		ImGui::ColorEdit3("Color Top", &colorTop.x);
		ImGui::ColorEdit3("Color Right", &colorRight.x);
		ImGui::ColorEdit3("Color Left", &colorLeft.x);
		if (ImGui::Button("Reset Color")) ResetColor();
	}

	if (ImGui::CollapsingHeader("Points position")) {
		ImGui::SliderFloat2("Top Position", &positionTop.x, -1.f, 1.0f);
		ImGui::SliderFloat2("Right Position", &positionRight.x, -1.f, 1.0f);
		ImGui::SliderFloat2("Left Position", &positionLeft.x, -1.f, 1.0f);
		if (ImGui::Button("Reset Position")) ResetPosition();
	}
	
	/*ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
		1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate
	);*/

	ImGui::End();
	//ImGui::PopStyleColor();
}

void Gui::InitGui(SDL_Window* sdlWin, SDL_GLContext context) {
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGui::StyleColorsDark();

	ImGui_ImplSDL2_InitForOpenGL(
		sdlWin,
		context);
	char* glsl_version = "#version 430";
	ImGui_ImplOpenGL3_Init(glsl_version);
}

void Gui::RenderGui(SDL_Window * sdlWin)
{
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
	SDL_GL_SwapWindow(sdlWin);
}

void Gui::ResetColor()
{
	colorTop = { 1.0f, 0.0f, 0.0f, 1.0f };
	colorRight = { 0.0f, 1.0f, 0.0f, 1.0f };
	colorLeft = { 0.0f, 0.0f, 1.0f, 1.0f };
}

void Gui::ResetPosition()
{
	positionTop = { 0.0f, 0.5f };
	positionRight = { 0.5f, -0.5f };
	positionLeft = { -0.5f, -0.5f };
}

void Gui::InitFrame(SDL_Window* sdlWin)
{
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplSDL2_NewFrame(sdlWin);
	ImGui::NewFrame();
}

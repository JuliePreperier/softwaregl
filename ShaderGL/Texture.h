#pragma once
#include <utility>
#include <string>
#include <GL/glew.h>
#include "../Algebra/Tga.h"
#include "../Algebra/Image.h"


struct Texture {
public:
	// Constructor
	Texture(const std::string& file) {
		Tga texture;
		ImageData imgTexture;
		texture.load(imgTexture, file.c_str());
		size_ = { imgTexture.x,imgTexture.y };

		glGenTextures(1,&texture_id_);
		glBindTexture(GL_TEXTURE_2D, texture_id_);
		// Assign the default for the texture
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glTexImage2D(
			GL_TEXTURE_2D, // Texture type.
			0, // Mipmap level (default: 0).
			GL_RGBA8, // Internal storage format.
			static_cast<GLsizei>(size_.first), // Width.
			static_cast<GLsizei>(size_.second), // Height.
			0, // This value must be 0.
			GL_RGBA, // Specify the pixel format.
			GL_FLOAT, // Specify the type.
			imgTexture.img.data()// Pointer to the array. -> give you the pointer of the 1st element
		); 

		glGenerateMipmap(GL_TEXTURE_2D); // generate the mipmap
	}

	// Destructor
	~Texture() {
		glDeleteTextures(1, &texture_id_);
	}

	void Bind(const unsigned int slot = 0) const;
	void UnBind() const;

private:
	unsigned int texture_id_ = 0;
	std::pair<size_t, size_t> size_ = { 0, 0 };
};
#include "Program.h"

Program::Program(const Shaders& vertex, const Shaders& frag)
{
	program_id_ = glCreateProgram();
	glAttachShader(program_id_, vertex.GetId());
	glAttachShader(program_id_, frag.GetId());
	glLinkProgram(program_id_);
}

Program::~Program()
{
	glDeleteProgram(program_id_);
}

void Program::Use()
{
	glUseProgram(program_id_);
}

void Program::UniformBool(const std::string& name, bool value) const
{
	glUniform1i(
		glGetUniformLocation(program_id_, name.c_str()), (int)value);
}

void Program::UniformInt(const std::string& name, int value) const
{
	glUniform1i(
		glGetUniformLocation(program_id_, name.c_str()),
		value);

}

void Program::UniformFloat(const std::string& name, float value) const
{
	glUniform1f(glGetUniformLocation(program_id_, name.c_str()), value);
}

void Program::UniformVector2(const std::string& name, const algebra::Vec2<float>& vec2) const
{
	glUniform2f(
		glGetUniformLocation(program_id_, name.c_str()), vec2.x, vec2.y);
}

void Program::UniformVector3(const std::string& name, const algebra::Vec3<float>& vec3) const
{
	glUniform3f(
		glGetUniformLocation(program_id_, name.c_str()),
		vec3.x,
		vec3.y,
		vec3.z);
}

void Program::UniformVector4(const std::string& name, const algebra::Vec4<float>& vec4) const
{
	glUniform4f(
		glGetUniformLocation(program_id_, name.c_str()),
		vec4.x,
		vec4.y,
		vec4.z,
		vec4.w);
}

void Program::UniformMatrix(const std::string& name, const algebra::Matrix4<float>& mat, const bool flip) const
{
	glUniformMatrix4fv(
		glGetUniformLocation(program_id_, name.c_str()),
		1,
		flip ? GL_TRUE : GL_FALSE,
		&mat.vec1.x);

}

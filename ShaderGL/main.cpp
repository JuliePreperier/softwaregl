#define _USE_MATH_DEFINES

#include <GL/glew.h>
#include "Window.h"
#include "Shaders.h"
#include "Gui.h"
#include "Texture.h"
#include "Program.h"
#include "../Algebra/Camera.h"
#include "../Algebra/Mesh.h"
#include <memory>

std::shared_ptr<Texture> texture1_ = nullptr;

int main(int argc, char* argv[])
{
	Window win;
	win.InitWindow(win.windowWidth, win.windowHeight);

	Mesh mesh;
	mesh.ExtractObj("../Assets/torus.obj");
	mesh.ComputeFlat();

	/* ERROR MESSAGE */
	glEnable(GL_DEBUG_OUTPUT);
	glDebugMessageCallback(
		Window::ErrorMessageHandler,
		nullptr);

	/* BUFFERS */

	// Index Buffer
	unsigned int indexBufferObject = 0;
	glGenBuffers(1, &indexBufferObject);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObject);
	glBufferData(
		GL_ELEMENT_ARRAY_BUFFER,
		mesh.GetFlatIndices().size() * sizeof(float),
		mesh.GetFlatIndices().data(),
		GL_STATIC_DRAW);

	// Vertex Buffer
	unsigned int vertexBufferObject = 0;
	glGenBuffers(1, &vertexBufferObject);
	glBindBuffer(GL_ARRAY_BUFFER,
		vertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER,
		mesh.GetFlatPositions().size() * sizeof(float),
		mesh.GetFlatPositions().data(),
		GL_STATIC_DRAW);

	// Normal Buffer
	unsigned int normalBufferObject = 0;
	glGenBuffers(1, &normalBufferObject);
	glBindBuffer(GL_ARRAY_BUFFER,
		normalBufferObject);
	glBufferData(GL_ARRAY_BUFFER,
		mesh.GetFlatNormals().size() * sizeof(float),
		mesh.GetFlatNormals().data(),
		GL_STATIC_DRAW);

	/* TEXTURE */

	GLuint texture_coordinate_object = 0;
	glGenBuffers(1, &texture_coordinate_object);
	glBindBuffer(GL_ARRAY_BUFFER, texture_coordinate_object);
	glBufferData(
		GL_ARRAY_BUFFER,
		mesh.GetFlatTextures().size() * sizeof(float),
		mesh.GetFlatTextures().data(),
		GL_STATIC_DRAW);

	/* SHADERS */

	GLuint vertexAttribObject = 0;
	glGenVertexArrays(1, &vertexAttribObject);
	glBindVertexArray(vertexAttribObject);
	
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	
	glBindBuffer(GL_ARRAY_BUFFER, normalBufferObject);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glBindBuffer(GL_ARRAY_BUFFER, texture_coordinate_object);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObject);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

	Shaders vertexShader(GL_VERTEX_SHADER);
	Shaders fragmentShader(GL_FRAGMENT_SHADER);

	if (!vertexShader.loadShader("../Assets/vertexShader.txt") ||
		!fragmentShader.loadShader("../Assets/fragmentShader.txt")) {
		exit(-1);
	}

	auto program = Program(
		vertexShader,
		fragmentShader);

	program.Use();
	const unsigned int slot = 0;
	
	texture1_ = std::make_shared<Texture>(
		"../Assets/Texture.tga");
	
	texture1_->Bind(slot);
	
	program.UniformInt("texture1", slot);
	
	// CAMERA
	algebra::Vec3<float> viewLookAt = { 0, 0, 1 };
	algebra::Vec3<float> viewUp = { 0, 1, 0 };
	algebra::Vec3<float> viewPosition = { 0, 0, -5 };
	algebra::Vec3<float> viewTarget = viewPosition + viewLookAt;
	Camera cam{ viewPosition, viewTarget, viewUp };
	
	algebra::Matrix4<float> projection = projection.perspectiveMatrix(win.windowHeight, win.windowWidth, 90 * M_PI / 180, 10000.0f, 0.1f);
	program.UniformMatrix("projection", projection);
	algebra::Matrix4<float> view = cam.View();
	program.UniformMatrix("view", view);
	algebra::Matrix4<float> model = {}; 
	program.UniformMatrix("model", model);

	// Enable blending to 1 - source alpha.
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Enable Z buffer.
	glEnable(GL_DEPTH_TEST);


	win.processWindow([&win](SDL_Event event)
	{
		switch (event.type) {
	
		case SDL_KEYDOWN:
			if (event.key.keysym.sym == SDLK_ESCAPE) {
				return false;
			}
			break;

		case SDL_QUIT:
			return false;
		}
		return true;
	},
		[&win, &mesh, &program]()
	{
		static auto start = std::chrono::system_clock::now();
		auto end = std::chrono::system_clock::now();
		std::chrono::duration<float> timer = end - start;

		algebra::Matrix4<float> rotationMatrix;

		algebra::Matrix4<float> mX;
		algebra::Matrix4<float> mY;
		algebra::Matrix4<float> mZ;

		mX = mX.rotateX(timer.count());
		mY = mY.rotateY(timer.count() * 0.7f);
		mZ = mZ.rotateZ(timer.count() * 0.5f);

		rotationMatrix = mX * mY * mZ;

		program.UniformMatrix("model", rotationMatrix);
		texture1_->Bind(0);
		
		glDrawElements(
			GL_TRIANGLES,  // What do we draw.
			mesh.GetFlatIndices().size(),// 3 points.
			GL_UNSIGNED_INT, // This HAS to be unsigned!
			nullptr);        // Delta to the beginning.

		return true;
	});

	return 0;
}
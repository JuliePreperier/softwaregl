#pragma once
#include "Shaders.h"
#include "../Algebra/Algebra.h"

struct Program {
public:
	// Constructor
	Program(const Shaders& vertex, const Shaders& frag);
	// Destructor
	virtual ~Program();
	// Use the program
	void Use();
	// Create a uniform from a string and a bool.
	void UniformBool(const std::string& name, bool value) const;
	// Create a uniform from a string and an int.
	void UniformInt(const std::string& name, int value) const;
	// Create a uniform from a string and a float.
	void UniformFloat(const std::string& name, float value) const;
	// Create a uniform from a string and a vector2.
	void UniformVector2(
		const std::string& name,
		const algebra::Vec2<float>& vec2) const;
	// Create a uniform from a string and a vector3.
	void UniformVector3(
		const std::string& name,
		const algebra::Vec3<float>& vec3) const;
	// Create a uniform from a string and a vector4.
	void UniformVector4(
		const std::string& name,
		const algebra::Vec4<float>& vec4) const;
	// Create a uniform from a string and a matrix.
	void UniformMatrix(
		const std::string& name,
		const algebra::Matrix4<float>& mat,
		const bool flip = false) const;

private:
	unsigned int program_id_ = 0;
	unsigned int vertex_id_ = 0;
	unsigned int fragment_id_ = 0;
};